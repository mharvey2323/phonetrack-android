PhoneTrack is een applicatie voor het continu loggen van locatiecoördinaten.
De toepassing werkt op de achtergrond. Punten worden opgeslagen op gekozen frequentie en
geüpload naar een server in real time. Deze logger werkt met
[https://gitlab.com/eneiluj/phonetrack-oc PhoneTrack Nextcloud app] of andere
aangepaste server (GET of POST HTTP-verzoeken). PhoneTrack app kan ook op afstand worden
aangestuurd door SMS-opdrachten.


# Functies

- Log naar meerdere bestemmingen met meerdere instellingen (frequentie, min. afstand, min. nauwkeurigheid)
- Log naar PhoneTrack Nextcloud app (PhoneTrack log job)
- Log naar elke server die HTTP GET- of POST-verzoeken kan ontvangen (aangepaste logopdracht)
- Sla posities op wanneer het netwerk niet beschikbaar is
Bestuur via SMS:
    - positie verkrijgen
    - alarm activeren
    - start alle logtaken
    - stop alle logtaken
    - maak een nieuw logboek aan
- Start bij het opstarten van het systeem
- Apparaten van een Nextcloud PhoneTrack-sessie op een kaart weergeven
- Donker thema
- Meertalige gebruikersinterface (vertaald op https://crowdin.com/project/phonetrack )

# Vereisten

Als je wilt inloggen op Nextcloud PhoneTrack app :

- Nextcloud instantie actief
- Nextcloud PhoneTrack app ingeschakeld

Anders, geen vereisten! (Behalve Android>=4.1)

# Alternatieven

Als deze app niet bevalt en je zoekt naar alternatieven: kijk eens naar de logmethoden/apps
in PhoneTrack wiki: https://gitlab.com/eneiluj/phonetrack-oc/wikis/userdoc#logging-methods .
